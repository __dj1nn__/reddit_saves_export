
# Reddit Saves Exporter

Currently the only endpoint that is supported is Pocket. If you want just the URLs and subreddits, you can just remove the pocket part in `run.py` and add a print loop.

**NOTE**: This does not check if you already have exported saves in your Pocket. This app is supposed to be a one time thing.

## Basic how to use:

1. Install [python](https://www.python.org/) 3.8+
2. Make sure [pip](https://pypi.org/project/pip/) is installed. This generally comes by default these days.
3. Make sure [git](https://git-scm.com/) is installed.
4. Run these commands for setting up repo:
    ```bash
    git clone https://gitlab.com/__dj1nn__/reddit_saves_export.git
    cd reddit_saves_export
    pip install -r requirements.txt
    ```
5. Go to [https://www.reddit.com/prefs/apps](https://www.reddit.com/prefs/apps) and log in.
6. Go down to the bottom of the page and click `Create another app`
7. Name it anything, use the script option, and enter anything for the redirect uri (it won't be used)
8. Copy the ID right under the name of the script to `API_KEY` in `.env`
9. Copy the secret to `API_SECRET` in `.env`
10. Enter you reddit username and password in the `REDDIT_USER` and `REDDIT_PASS` fields in `.env`
11. Go to [https://getpocket.com/developer/](https://getpocket.com/developer/), click `Create New App`, and log in if you haven't already.
12. Name the application whatever and give it any description.
13. Select the Add and Modify permissions if you only plan on using this key for this app. (I gave mine all permissions, so I could test with retrieval).
14. Select the platform you're on.
15. Accept the terms and click `Create application`.
16. Copy the consumer key to `POCKET_KEY` in `.env`.
17. Go to [this app](https://reader.fxneumann.de/plugins/oneclickpocket/auth.php) for authorizing the consumer key with your Pocket account.
18. Enter your consumer key and click `Absenden`.
19. Now log in and authorize your app.
20. You should be given an access token. Copy that to `POCKET_TOKEN` in `.env`.
21. Now you should have all the parameters you need. If you have 2 factor authentication enabled on your reddit account (which you [should](https://secureswissdata.com/two-factor-authentication-importance/)), add the code from your 2FA app to `2FA` in `.env` right before running.
22. Run `python src/run.py` (note some environments might require `python3 src/run.py`)
23. It will take some time, so grab a coffee.
24. You should have all of your reddit saves in Pocket tagged with `reddit` and the subreddit it came from.
25. Now if you want to set up new saves to automatically get exported to Pocket, you can set up something like [IFTTT](https://ifttt.com). There are some ready made, but as far as I can tell they don't add a subreddit tag, so I made one myself. Unfortunately, IFTTT doesn't let normal users share their applets though.