
from reddit_saves_export.reddit import Reddit
from reddit_saves_export.pocket import MyPocket
from decouple import config

if __name__ == '__main__':
    USERNAME = config('REDDIT_USER')
    PASSWORD = config('REDDIT_PASS')
    twofa = config('2FA', default=None)
    if not (twofa is None or twofa == ''):
        PASSWORD += ':' + twofa

    API_KEY = config('API_KEY')
    API_SECRET = config('API_SECRET')

    CONSUMER_KEY = config('POCKET_KEY')
    ACCESS_TOKEN = config('POCKET_TOKEN')

    print("Connecting to reddit.....")
    reddit = Reddit(USERNAME, PASSWORD, API_KEY, API_SECRET)
    
    print("Getting reddit saves.....")
    saves = reddit.get_saves()

    print("Connecting to pocket.....")
    pocket = MyPocket(CONSUMER_KEY, ACCESS_TOKEN)

    print("Adding saves to pocket.....")
    pocket.add_saves(saves)

    print("Done :)")