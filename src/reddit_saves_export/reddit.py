
import praw

class Reddit:
    def __init__(self, username, password, api_key, api_secret):
        self.reddit = praw.Reddit(client_id=api_key, client_secret=api_secret,
                                  username=username, password=password,
                                  user_agent="Python")

    def get_saves(self):
        return [{'url': f'https://reddit.com{post.permalink}', 'subreddit': post.subreddit.display_name} for post in self.reddit.user.me().saved(limit=None, params={'type':'links'})]
        