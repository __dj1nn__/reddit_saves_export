
from pocket import Pocket, PocketException
import time

class MyPocket:
    def __init__(self, consumer_key, access_token):
        self.pocket = Pocket(consumer_key=consumer_key, access_token=access_token)

    def add_saves(self, saves):
        for save in saves:
            self.pocket.add(save['url'], tags=["reddit", save['subreddit']])
